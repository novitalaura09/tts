#!/bin/bash

WORD_SPEED="135" # per min (min: 80, max: 450, default: 175)
VOICE_PROFILE="en+f5"
PITCH="60" # (min: 0, max: 99, default: 50)
WORD_PAUSE="0" # millisecond (default: 0)
AMPLITUDE="80" # (min: 0, max: 200, default: 100)




# execute
espeak-ng -v "$VOICE_PROFILE" \
	-a "$AMPLITUDE" \
	-s "$WORD_SPEED" \
	-g "$WORD_PAUSE" \
	-p "$PITCH" \
	"${1:-"Excuse me. What do you want me to speak again?"}"
